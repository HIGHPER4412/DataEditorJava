import java.io.*;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

public class Main {
    public static void main(String[] args) {
      //Main main = new Main();
      Scanner userInput = new Scanner(System.in);
      FMain userFile = new FMain();
      while(userFile.machineRepeat) {
        try {
        System.out.println("(0) Open");
        System.out.println("(1) Edit");
        System.out.println("(2) Add to a File");
        System.out.println("(3) Search");
        System.out.println("(4) Show A File");
        System.out.println("(5) Exit");
        System.out.print("Choose the Operation want to Use: ");
        int choice = userInput.nextInt();
        switch (choice)
          {
            case 0:
              userFile.Open();
              break;
            case 1:
              userFile.Edit();
              break;
            case 2:
              userFile.Add();
              break;
            case 3:
              userFile.Search();
              break;
            case 4:
              userFile.Show();
              break;
            case 5:
              userFile.Exit();
              break;
            default:
              System.out.println("Please Enter 1 - 5");
              break;
          }
        } catch (InputMismatchException e){ 
            userFile.machineRepeat = false;
            System.out.println("Error Closing a program " + e);
        }
      }
      System.out.println("Thanks Your Using!");

      userInput.close();
    }
  }
  class FMain extends FunctionEdit{
      public boolean machineRepeat = true;
      private File userMainFile;
      private FunctionEdit edit;
      private BufferedWriter writer;
      private ArrayList<String> storeStringFile = new ArrayList<>();
      private String SearchString = "";
      private BufferedReader StringFile;
      private String Found;
      private int userEdit;
      Scanner userInputIO = new Scanner(System.in);
      //Must Modify Array
      public void Open()
      {
        userMainFile = new File("MainF.txt");
        if(!userMainFile.exists())
        {
          System.out.println("File Not found!");
        } else {
          System.out.println("File found!");
        }
      }
      public void Edit()
      {
        edit = new FunctionEdit();
        System.out.println("(1) Remove");
        System.out.println("(2) Change");
        System.out.print("Enter A Chooses: ");
        userEdit = userInputIO.nextInt();
        switch (userEdit)
        {
          case 1:
            edit.Remove(storeStringFile, userMainFile);
            break;
          case 2:
            edit.Change(storeStringFile, userMainFile);
            break;
          default:
            System.out.println("Please Enter 1 - 2");
            break;
        }
      }
      public void Add()
      {
        try {
          writer = new BufferedWriter(new FileWriter(userMainFile, true));
          System.out.print("Enter a String: ");
          String AddUser = userInputIO.next();
          writer.append(AddUser);
          writer.close();
        } catch (IOException e)
        {
          System.out.println(e);
        }
      }
      public void Search()
      {
        System.out.print("Search: ");
        String userString = userInputIO.nextLine();
        int x = 0;
        try {
          StringFile = new BufferedReader(new FileReader(userMainFile));
          while((SearchString = StringFile.readLine()) != null)
          {
            if(SearchString.trim().equalsIgnoreCase(userString))
            {
              x = 1;
            }
          }
         Found = (x == 1) ? "Found It" : "Not Found It";
         System.out.println("\n" + Found + "\n");
         StringFile.close();
        } catch (FileNotFoundException e)
        {
          System.err.println(e);
        } catch (NullPointerException e)
        {
          System.err.println(e);
        } catch (IOException e)
        {
          System.err.println(e);
        } catch (NoSuchElementException e)
        {
          System.err.println(e);
        }
      }
      public void Show()
      {
        try {
          Scanner reader = new Scanner(userMainFile);
          while(reader.hasNextLine())
          {
            System.out.println(reader.nextLine());
          }
          reader.close();
        } catch (FileNotFoundException e)
        {
          System.err.println(e);
        } catch (NullPointerException e)
        {
          System.out.println("File Never Been Opened!");
        }
      }
      public void Exit()
      {
        try {
          Scanner userMInput = new Scanner(userMainFile);
          userMInput.close();
          machineRepeat = false;
          edit.FunctionEditIn.close();
          userInputIO.close();
        } catch (FileNotFoundException e) {
          System.out.println("File Never been Opened");
        } catch (NullPointerException e) {
          machineRepeat = false;
          System.out.println("File Never Been Opened!!");
        }
      }
    };
  class FunctionEdit {
    private String StrRemover;
    //RemoveF
    private BufferedWriter WriterFile;
    private BufferedReader ReaderFile;
    Scanner FunctionEditIn = new Scanner(System.in);
    protected void Remove(ArrayList<String> storeArrayList, File userMainFile)
    {
      int j = 1;
      try {
        storeArrayList = new ArrayList<>();
        ReaderFile = new BufferedReader(new FileReader(userMainFile));
        while((StrRemover = ReaderFile.readLine()) != null)
        {
          storeArrayList.add(StrRemover);
          System.out.println(j + ": " + StrRemover);
          j++;
        }
        ReaderFile.close();
        //Close the Reader
        System.out.print("Enter a Number you want to Remove: ");
        int NChoose = FunctionEditIn.nextInt();
        storeArrayList.remove(NChoose - 1);

        WriterFile = new BufferedWriter(new FileWriter(userMainFile, false));
        for(int i = 0; i < storeArrayList.size(); i++)
        {
          WriterFile.write(storeArrayList.get(i));
          WriterFile.newLine();
        }
        storeArrayList.clear();
        ReaderFile.close();
        WriterFile.close();
        }catch (IOException e) {
          e.printStackTrace();
        }catch(IndexOutOfBoundsException e) {
          System.out.println("Its Out of Bounds!" + e);
        }catch(NoSuchElementException e)
        {
          e.printStackTrace();
        }

    }
    protected void Change(ArrayList<String> storeStringFile, File userMainFile)
    {
      try {
        storeStringFile = new ArrayList<String>();
        BufferedReader fileReader = new BufferedReader(new FileReader(userMainFile));
        String StrStore;
        int i = 1;
        System.out.println();
        while((StrStore = fileReader.readLine()) != null)
        {
          storeStringFile.add(StrStore);
          System.out.println(i + ": " + StrStore);
          i++;
        }
        System.out.println();
        fileReader.close();
        System.out.print("Enter a Number to Change 1 - " + storeStringFile.size() + " : ");
        int changeUser = FunctionEditIn.nextInt();
        String changeStr = storeStringFile.get(changeUser - 1);
        System.out.print(changeStr + " Change to: ");
        FunctionEditIn.nextLine();
        changeStr = FunctionEditIn.nextLine();
        storeStringFile.set((changeUser - 1), changeStr);
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(userMainFile, false));
        for(int j = 0; j < storeStringFile.size(); j++)
        {
          fileWriter.write(storeStringFile.get(j));
          fileWriter.newLine();
        }
        storeStringFile.clear();
        fileWriter.close();
      } catch (IOException e)
      {
        e.printStackTrace();
      } catch (IndexOutOfBoundsException e)
      {
        e.printStackTrace();
      }
    }
  };
